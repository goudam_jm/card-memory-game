package com.mak.cardmemorygame;

import android.app.AlertDialog;
import android.content.res.AssetManager;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;

public class CardMemoryGame extends AppCompatActivity implements OnClickListener, AnimationListener {

    private final int MAX_CARDS_FOR_GAME = 20;
    private final String IMAGE_PATH = "cardimages";
    private final String TAG = "CARD_MEMORY_GAME";

    private int score;
    private int cardsToFind;
    private ImageView previous;
    private ImageView current;
    private ArrayList<String> cardsTotal;
    private HashMap<ImageView, Boolean> imageViewMap;
    private HashMap<ImageView, String> gameCardsMap;

    private TextView scoreTextView;
    private Animation animation1;
    private Animation animation2;
    private AssetManager assets;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_memory_game);

        // initialize the variables
        assets = getAssets();
        imageViewMap = new HashMap<>();
        cardsTotal = new ArrayList<>();
        cardsTotal = new ArrayList<>();
        gameCardsMap = new HashMap<>();
        resetVariables();

        scoreTextView = (TextView) findViewById(R.id.scoreTextView);

        // set keys and values in imageViewMap
        resetImageViewMap();

        // start new game
        newGame();

        // set onclick listener for all the imageViews
        for(ImageView iv: imageViewMap.keySet()){
            iv.setOnClickListener(this);
        }

        // set animation listeners
        animation1 = AnimationUtils.loadAnimation(this, R.anim.to_middle);
        animation1.setAnimationListener(this);

        animation2 = AnimationUtils.loadAnimation(this, R.anim.from_middle);
        animation2.setAnimationListener(this);
    }

    private void resetImageViewMap(){
        imageViewMap.put((ImageView) findViewById(R.id.imageView1), true);
        imageViewMap.put((ImageView) findViewById(R.id.imageView2), true);
        imageViewMap.put((ImageView) findViewById(R.id.imageView3), true);
        imageViewMap.put((ImageView) findViewById(R.id.imageView4), true);
        imageViewMap.put((ImageView) findViewById(R.id.imageView5), true);
        imageViewMap.put((ImageView) findViewById(R.id.imageView6), true);
        imageViewMap.put((ImageView) findViewById(R.id.imageView7), true);
        imageViewMap.put((ImageView) findViewById(R.id.imageView8), true);
        imageViewMap.put((ImageView) findViewById(R.id.imageView9), true);
        imageViewMap.put((ImageView) findViewById(R.id.imageView10), true);
        imageViewMap.put((ImageView) findViewById(R.id.imageView11), true);
        imageViewMap.put((ImageView) findViewById(R.id.imageView12), true);
        imageViewMap.put((ImageView) findViewById(R.id.imageView13), true);
        imageViewMap.put((ImageView) findViewById(R.id.imageView14), true);
        imageViewMap.put((ImageView) findViewById(R.id.imageView15), true);
        imageViewMap.put((ImageView) findViewById(R.id.imageView16), true);
        imageViewMap.put((ImageView) findViewById(R.id.imageView17), true);
        imageViewMap.put((ImageView) findViewById(R.id.imageView18), true);
        imageViewMap.put((ImageView) findViewById(R.id.imageView19), true);
        imageViewMap.put((ImageView) findViewById(R.id.imageView20), true);
    }

    private void resetVariables(){
        score = 0;
        cardsToFind = MAX_CARDS_FOR_GAME;
        current = null;
        previous = null;
        cardsTotal.clear();
        gameCardsMap.clear();
        for(ImageView iv: imageViewMap.keySet()){
            iv.setVisibility(View.VISIBLE);
        }
    }

    private void newGame(){
        loadCards();
        assignCardsToGame();
        setScoreTextView();
    }

    private void setScoreTextView(){
        scoreTextView.setText(""+score);
    }

    private void loadCards(){
        try{
            for(String el: assets.list(IMAGE_PATH)){
                cardsTotal.add(el);
            }
        }
        catch(IOException ioe){
            Log.e(TAG, "IO Exception: loadCards", ioe);
        }
    }

    private void shuffleCards(ArrayList<String> items){
        long seed = System.nanoTime();
        Collections.shuffle(items, new Random(seed));
    }

    private void assignCardsToGame(){
        int i = 0;
        ArrayList<String> tempCardStack = new ArrayList<>();
        for(i = 0; i < MAX_CARDS_FOR_GAME/2; ++i){
            tempCardStack.add(cardsTotal.get(i));
            tempCardStack.add(cardsTotal.get(i));
        }
        shuffleCards(tempCardStack);
        for(ImageView iv: imageViewMap.keySet()){
            gameCardsMap.put(iv, tempCardStack.remove(0));
        }
    }

    private Drawable getCardImage(String cardImageName){
        Drawable cardImage = null;

        try{
            InputStream stream = assets.open(IMAGE_PATH + "/" + cardImageName);

            // load the asset as a Drawable and return the cardImage
            cardImage = Drawable.createFromStream(stream, cardImageName);
        }
        catch(IOException ioe){
            Log.e(TAG, "IO Exception: getCardImage", ioe);
        }
        return cardImage;
    }

    private void checkGame(){
        if(previous != null){
            if(gameCardsMap.get(current) == gameCardsMap.get(previous)){
                previous.setVisibility(View.INVISIBLE);
                current.setVisibility(View.INVISIBLE);

                previous.setImageResource(R.drawable.b1fv);
                current.setImageResource(R.drawable.b1fv);

                Toast toast = Toast.makeText(getApplicationContext(), "Matching!", Toast.LENGTH_LONG);
                toast.show();

                score++;
                setScoreTextView();

                cardsToFind-=2;

                if(cardsToFind == 0){
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                    alertDialog.setTitle(R.string.new_game);
                    alertDialog.setMessage("Your score: "+score);
                    alertDialog.setPositiveButton( R.string.OK, null);

                    AlertDialog alert = alertDialog.create();
                    alert.show();
                    resetImageViewMap();
                    resetVariables();
                    newGame();
                }
            }
            else{
                imageViewMap.put(previous, true);
                previous.setImageResource(R.drawable.b1fv);
                current.setAnimation(animation1);
                current.startAnimation(animation1);

                Toast toast = Toast.makeText(getApplicationContext(), "Not Matching!", Toast.LENGTH_LONG);
                toast.show();

                score--;
                setScoreTextView();
            }
            previous = null;
        }
        else{
            previous = current;
        }
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        if(imageViewMap.get(current)){
            current.setImageDrawable(getCardImage(gameCardsMap.get(current)));
            imageViewMap.put(current, !imageViewMap.get(current));
            checkGame();
        }
        else{
            imageViewMap.put(current, !imageViewMap.get(current));
            current.setImageResource(R.drawable.b1fv);
        }
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    @Override
    public void onClick(View v) {
        current = (ImageView) findViewById(v.getId());
        current.clearAnimation();
        current.setAnimation(animation1);
        current.startAnimation(animation1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_card_memory_game, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
